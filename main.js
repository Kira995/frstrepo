let list = new Todolist();
let audiodone = new Audio('Pression.mp3');
let audioout = new Audio('air.wav');
let audioin = new Audio('swish.mp3');
let ul;
let divlist = document.getElementById('divlist');
if(localStorage.getItem('list') != null){
ul = document.createElement('ul');
ul.innerHTML = localStorage.getItem('list');
divlist.appendChild(ul);
list.tasklist = JSON.parse(localStorage.getItem('array'));
}
else{
ul = document.createElement('ul');
divlist.appendChild(ul);
}
//per far funzionare bottoni preesistenti
if(document.getElementsByClassName('dltbtn')){
for(let i = 0; i<list.tasklist.length; i++){
     document.getElementsByClassName('dltbtn')[i].onclick = (event)=>{
     audioout.play();
     let btn = event.target.parentElement;
     list.rmTask(btn.parentElement.children[1].textContent);
     btn.parentElement.remove();
     localStorage.setItem('list', ul.innerHTML);
     localStorage.setItem('array', JSON.stringify(list.tasklist)); 
}}}
//per far funzionare paragrafi preesistenti funzione done
if(document.getElementsByTagName('p')){
    for(let i = 0; i<list.tasklist.length; i++){
    document.getElementsByTagName('p')[i].onclick = ()=>{
    audiodone.play();
    let x = 0;
    let find = 'false';
        while(find != 'true'){
        if(list.tasklist[x].description == document.getElementsByTagName('p')[i].textContent)
    {
        list.tasklist[x].done = list.Done(list.tasklist[x].done);
        if(list.tasklist[x].done == 'true'){
        document.getElementsByTagName('p')[i].style.textDecoration = 'line-through';
        document.getElementsByTagName('p')[i].style.textDecorationColor = 'white';
        }
        else
        document.getElementsByTagName('p')[i].style.textDecoration = 'none';
        find = 'true';
    }
        let done = document.getElementsByTagName('p')[i].parentElement.firstChild;
        done.firstChild.innerHTML = '<img width=40px height=40px>';
        if(list.tasklist[x].done == 'true'){
        done.firstChild.src =' done.png';
        done.firstChild.style.display = 'block';
        }
        else
        done.firstChild.style.display = 'none';
    }
    localStorage.setItem('array', JSON.stringify(list.tasklist));  
}}}
document.addEventListener("keyup", (event)=> {
    // Numero 13 equivale a 'enter' su tastiera
    if (event.keyCode === 13) {
      // annulla il default se necessario
      event.preventDefault();
      // handler che clicca sul pulsante trigger
      document.getElementsByClassName('addtask').namedItem('addtask').click();
    }
  });
document.getElementsByClassName('addtask').namedItem('addtask').onclick = ()=> {
    if(document.getElementsByClassName('task').item('task').value == '')
    alert('Inserisci un valore nel campo');
    else{
    audioin.play();
    list.addTask(document.getElementsByClassName('task').item('task').value);
    let line = document.createElement('li');
    let p = document.createElement('p');
    p.textContent = document.getElementsByClassName('task').item('task').value;
    document.getElementsByClassName('task').item('task').value = null;
    let dltbtn = document.createElement('button');
    dltbtn.className = 'dltbtn';
    dltbtn.name = 'dltbtn';
    dltbtn.innerHTML = '<img src="delete.png" width=40 px height=40px>';
    let status = document.createElement('div');
    status.id = 'done';
    status.innerHTML = '<img width=40px height=40px>';
    status.firstChild.style.display = 'none';
    line.appendChild(status);
    p.contentEditable = 'true';
    line.appendChild(p);
    line.appendChild(dltbtn);
    ul.appendChild(line);
    p.onclick = (event)=>{
        audiodone.play();
        let x = 0;
        let div;
        let find = 'false';
            while(find != 'true'){
            if(list.tasklist[x].description == event.target.textContent)
        {
            list.tasklist[x].done = list.Done(list.tasklist[x].done);
            find = 'true';
            if(list.tasklist[x].done == 'true'){
            event.target.style.textDecoration = 'line-through';
            event.target.style.textDecorationColor = 'white';
            }
            else
            event.target.style.textDecoration = 'none';
        }
            div = event.target.parentElement.firstChild;
            if(list.tasklist[x].done == 'true'){
            div.firstChild.style.display = 'block';
            div.firstChild.src = 'done.png';
            }
            else
            div.firstChild.style.display = 'none';
            x++;
        }
        console.log(list.tasklist);
        localStorage.setItem('list', ul.innerHTML);
        localStorage.setItem('array', JSON.stringify(list.tasklist));  
    }
    console.log(list.tasklist);
    dltbtn.onclick = ()=>{
        audioout.play(); 
        list.rmTask(dltbtn.parentElement.children[1].textContent);
        dltbtn.parentElement.remove();  
        console.log(list.tasklist);
        localStorage.setItem('list', ul.innerHTML);
        localStorage.setItem('array', JSON.stringify(list.tasklist));
    } 
    localStorage.setItem('list', ul.innerHTML);
    localStorage.setItem('array', JSON.stringify(list.tasklist));
}}
document.getElementsByClassName('clear').namedItem('clear').onclick = ()=>{
    ul = list.clearList(ul);
    ul = document.createElement('ul');
    divlist.appendChild(ul);
    for(let i=0; i<list.tasklist.length; i=0)
        list.tasklist.shift();
    console.log(list.tasklist);
    localStorage.setItem('list', ul.innerHTML);
    localStorage.setItem('array', JSON.stringify(list.tasklist));
} 
//Orologio
function refreshClock (){
    let clock = document.getElementById('clock');
    let ora = new Date;
    let h,m,s;
    h = ora.getHours();
    if(h<10)
    h = '0' + h;
    m = ora.getMinutes();
    if(m<10)
    m = '0' + m;
    s = ora.getSeconds();
    if(s<10)
    s = '0' + s;
    data = h + ':' + m + ':' + s;
    clock.textContent = data;
}
//loop Orologio
setInterval(refreshClock, 1000);